#!/bin/bash

DO_DELETE=$1

# move files from 'preview' to 'load'
docker compose run --rm \
    -e PIPELINE_ACTION=publish_names \
    publisher

# set API 'busy'
docker compose run --rm \
    -e ES_CONTROL_COMMAND=set_api_status \
    -e ES_CONTROL_ARGUMENT=busy \
    document_loader

if [[ "$DO_DELETE" == "--delete-existing" ]]; then

    echo "deleting existing documents"

    # delete all documents in names
    docker compose run --rm \
        -e ES_ACTIVE_INDEX=names \
        -e ES_CONTROL_COMMAND=delete_documents \
        document_loader

fi

# load general name documents
docker compose run --rm \
    -e ES_ACTIVE_INDEX=names \
    -e ES_CONTROL_COMMAND=load_documents  \
    -e ES_CONTROL_ARGUMENT=/data/documents/load/names \
    document_loader

#  set API 'ready'
docker compose run --rm \
    -e ES_CONTROL_COMMAND=set_api_status \
    -e ES_CONTROL_ARGUMENT=ready \
    document_loader
