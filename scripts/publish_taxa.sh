#!/bin/bash

DO_DELETE=$1

# move files from 'preview' to 'load'
sudo docker compose run --rm \
    -e PIPELINE_ACTION=publish_taxa \
    publisher

# set API 'busy'
sudo docker compose run --rm \
    -e ES_CONTROL_COMMAND=set_api_status \
    -e ES_CONTROL_ARGUMENT=busy \
    document_loader

if [[ "$DO_DELETE" == "--delete-existing" ]]; then

    echo "deleting existing documents"

    # delete documents in taxa
    sudo docker compose run --rm \
        -e ES_ACTIVE_INDEX=taxa \
        -e ES_CONTROL_COMMAND=delete_documents \
        document_loader

fi

# load taxon documents
sudo docker compose run --rm \
    -e ES_ACTIVE_INDEX=taxa \
    -e ES_CONTROL_COMMAND=load_documents  \
    -e ES_CONTROL_ARGUMENT=/data/documents/load/taxa \
    document_loader

#  set API 'ready'
sudo docker compose run --rm \
    -e ES_CONTROL_COMMAND=set_api_status \
    -e ES_CONTROL_ARGUMENT=ready \
    document_loader
