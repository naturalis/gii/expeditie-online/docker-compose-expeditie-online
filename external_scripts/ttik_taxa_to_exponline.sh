#!/bin/bash

HOST_DIR=/data/linnaeus/mysqllog/taxa/

date

mkdir $HOST_DIR

echo "exporting ttik taxa tables"
sudo docker exec docker-linnaeusng_db_1 bash -c 'mysqldump --add-drop-table linnaeus_ng content_taxa languages name_types names nsr_ids pages_taxa pages_taxa_titles projects_ranks ranks taxa > /var/log/mysql/taxa/ttik_taxa.sql'

ls -l $HOST_DIR

echo "mirroring to expeditie online server"
/home/development/mc/mc mirror --overwrite $HOST_DIR expeditie_online/ttik_taxa/

echo "mirroring to expeditie online dev server"
/home/development/mc/mc mirror --overwrite $HOST_DIR expeditie_online_dev/ttik_taxa/

