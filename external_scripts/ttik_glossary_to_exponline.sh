#!/bin/bash

HOST_DIR=/data/linnaeus/mysqllog/glossary/

date

mkdir $HOST_DIR

echo "exporting ttik glossary tables"
#sudo docker exec docker-linnaeusng_db_1 bash -c 'mysqldump --add-drop-table linnaeus_ng glossary glossary_media glossary_media_captions glossary_synonyms > /var/log/mysql/glossary/glossary.sql'
sudo docker exec docker-linnaeusng_db_1 bash -c 'mysqldump --add-drop-table linnaeus_ng glossary glossary_synonyms media media_captions media_metadata media_modules > /var/log/mysql/glossary/glossary.sql'

ls -l $HOST_DIR 

echo "mirroring to expeditie online server"
/home/development/mc/mc mirror --overwrite $HOST_DIR expeditie_online/ttik_glossary/

echo "mirroring to expeditie online dev server"
/home/development/mc/mc mirror --overwrite $HOST_DIR expeditie_online_dev/ttik_glossary/


echo "done"

