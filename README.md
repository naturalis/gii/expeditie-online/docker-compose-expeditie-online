# Docker Compose Expeditie Online

The Docker images for the individual components are been maintained as separate repositories, where they are scanned, security checked and pushed to the registry. Therefore, the purpose of this repo is solely to deploy the docker compose file combining these components.

By default, the `latest` versions of the individual images are used. For the development server, this can be overrided by providing the correct tag names in the variables `ADMIN_IMAGE_VERSION`, `HARVESTER_IMAGE_VERSION`, `PIPELINE_IMAGE_VERSION` or `DOCUMENT_LOADER_IMAGE_VERSION` at the [Run pipeline](https://gitlab.com/naturalis/gii/expeditie-online/docker-compose-expeditie-online/-/pipelines/new) page.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`
